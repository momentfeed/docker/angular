FROM node

RUN set -ex; \
    npm install -g @angular/cli@10; \
    apt-get update; \
    apt-get install -y apt-transport-https; \
    curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add -; \
    echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list; \
    apt-get update; \
    apt-get install -y google-chrome-stable; \
    rm -rf /var/lib/apt/lists/*; \
    rm -rf ~/.npm/

ENV CHROME_BIN /usr/bin/google-chrome

USER node
